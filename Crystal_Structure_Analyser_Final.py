import math as m
n = int(input("Enter number of examples to check\n"))
# taking value for no of iterations to do
for i in range(n):
    d1 = float(input("Enter theta for (1,0,0) maxima in degrees\n"))
    # input thetas(variable name is d for space saving as that varibale becomes d later)
    d2 = float(input("Enter theta for (1,1,0) maxima in degrees\n"))
    d3 = float(input("Enter theta for (1,1,1) maxima in degrees\n"))
    # calculate d1,d2 and d3 from thetas
    d1, d2, d3 = (1/(2*m.sin(m.radians(d1)))), (1 /
                                                (2*m.sin(m.radians(d2)))), (1/(2*m.sin(m.radians(d3))))
    # take ratio of d1:d2:d3
    d2 /= d1
    d3 /= d1
    d1 = 1
    # round off to 1 decimal(as there may be an error till 2nd decimal)
    d1, d2, d3 = round(d1, 1), round(d2, 1), round(d3, 1)
    # check which d1 d2 and d3 are equal to which entry in data table
    if((d2 == round(1/m.sqrt(2), 1)) and (d3 == round(1/m.sqrt(3), 1))):
        print("The Given Crystal is Simple Cubic\n")
    elif((d2 == round(1/m.sqrt(2), 1)) and (d3 == (2/round(1/m.sqrt(3), 1)))):
        print("The Given Crystal is Body Centered Cubic\n")
    elif((d2 == round(m.sqrt(2), 1)) and (d3 == round(1/m.sqrt(3), 1))):
        print("The Given Crystal is Face Centered Cubic\n")
    else:
        print("Data doesnt match any of SCC,BCC or FCC\n")
    # Giving user the option to exit
    if(n > 1):
        ch = input("Do you want to quit?(Y for YES, N for NO)\n")
        if(ch == 'Y' or ch == 'y'):
            break
